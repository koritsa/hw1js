Теоретичні питання:

1 Як можна оголосити змінну у Javascript?

Відповідь:
За допомогою змінних var(зараз не використовується), let або const.
let age = 25; де
age - це назва змінної,
= - це оператор присвоєння,
25 - це значення змінної.

2 У чому різниця між функцією prompt та функцією confirm?

Відповідь:
confirm - функція показує модальне вікно з питанням та двома кнопками: ОК та Скасувати. Результат: true, якщо натиснути кнопку Ок, інакше - false.
prompt - показує повідомлення з проханням ввести текст. Повертає цей текст або null, якщо натиснута кнопка Скасувати або клавіша Esc.

3 Що таке неявне перетворення типів? Наведіть один приклад.

Відповідь:
Неявне перетворення типів - це коли перетворення між типами проходить автоматично. Найчастіше це відбувається коли застосовують оператори до значень різних типів.
+"123"; // 123
alert(1 + "2"); // "12"
